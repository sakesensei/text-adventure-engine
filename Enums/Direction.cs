// The six default directions where the player can move to

namespace Enums
{
	public enum Direction
	{
		north,
		south,
		east,
		west,
		up,
		down
	}
}