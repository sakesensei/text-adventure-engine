﻿// The standard verbs the player can use, either by themselves or on something

namespace Enums
{
	public enum Verbs
	{
		go,
		open,
		close,
		attack,
		pick,
		drop,
		use,
		inventory,
		inspect,
		wait,
		quit,
		save,
		load,
		help,
		clear
	}
}
