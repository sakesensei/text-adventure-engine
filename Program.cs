﻿using System;
using System.Collections.Generic;

using Enums;
using GameElements;
using GameSystem;

namespace TextAdventureEngine
{
	class MainClass
	{
		public static void Main(string[] args)
		{
			#region Initialize

			// Initialize World and Rooms
			List<Room> World = new List<Room>();

			// Initialize Player
			Player player = new Player();

			// Create Rooms
			// Exemple: Room mainRoom = new Room("First Room", "Short Description", "Long Description");
			


			// Starting room:
			// Room currentRoom = mainRoom;

			// Put Rooms in the World
			// Exemple:  World.Add(mainRoom);

			// Create Room Exits
			// Exemple: mainRoom.Exits.Add(Direction.north, secondRoom);


			// Create Items
			// Exemple:
			// Item itemRock = new Item(true, false, "rock", "It's a rock.");
			

			// Put 'Items' inside 'Rooms' here
			// Exemple: mainRoom.Items.Add("rock", itemRock)

			// Init Variables
			string currentMessage = currentRoom.FirstDescription; // Defaults the introduction to be the first room's long description. 
			// TODO
			// Make this a global game description, maybe

			bool isPlaying = true; // Ends the game once it's false - 'quit' command
			bool isNewRoom = true; // Gives a more in-depth description if it's the first time visiting the room
			string playerInput; // The actual input command

			string[] inputArray = new string[4];

			#endregion

			#region Main Loop
			// ----- MAIN LOOP ----- //

			while (isPlaying)
			{
				if (isNewRoom)
				{
					// Name the room
					Message.Name(currentRoom);
					// Full room description
					Message.Description(currentMessage);
					// Items in the room
					if (!currentRoom.Items.GetEnumerator().MoveNext())
					{
						foreach (var roomItems in currentRoom.Items)
						{
							Message.Description($"{roomItems.Value.Name} {roomItems.Value.Place}");
						}
					}
				}
				else
				{
					if (!string.IsNullOrEmpty(currentMessage))
					{
						Message.Description(currentMessage);
					}
				}

				// Get Console Command
				Console.Write("\n> ");
				playerInput = Console.ReadLine();

				Command.GetInput(playerInput, out inputArray);
				Command.Action(inputArray, out string verb, out string target);

				if (playerInput != string.Empty)
				{
					if (Enum.TryParse<Verbs>(verb, out Verbs action))
					{
						switch (action)
						{
							#region Movement
							case Verbs.go:
								if (Command.IsValid(target) && Enum.TryParse<Direction>(target, out Direction exit))
								{
									currentRoom.Exits.TryGetValue(exit, out Room destination);
									if (destination != null)
									{
										currentMessage = (destination.IsFirstTime) ? destination.FirstDescription : destination.Description;
										destination.IsFirstTime = false;

										currentRoom = destination;
										isNewRoom = true;
									}
									else
									{
										currentMessage = Command.ErrorMessage(action);
										isNewRoom = false;
									}
								}
								else
								{
									currentMessage = $"Go where?";
									isNewRoom = false;
								}
								break;
							#endregion
							#region Pick up
							case Verbs.pick:
								if (Command.IsValid(target))
								{
									if (currentRoom.Items.TryGetValue(target, out Item pickItem))
									{
										if (pickItem.IsPickable)
										{
											currentRoom.Items.Remove(pickItem.Name);
											player.Inventory.Add($"{pickItem.Name}", pickItem);
											currentMessage = $"Picked up {pickItem.Name}";
										}
										else
										{
											currentMessage = Command.ErrorMessage(action);
										}
									}
									else
									{
										currentMessage = "Pick what?";
									}
									isNewRoom = false;
								}
								else
								{
									currentMessage = "Pick what?";
									isNewRoom = false;
								}
								break;
							#endregion
							#region Drop
							case Verbs.drop:
								if (Command.IsValid(target))
								{
									if (player.Inventory.TryGetValue(target, out Item dropItem))
									{
										// Let's assume the player can drop anything

										player.Inventory.Remove(dropItem.Name);
										currentRoom.Items.Add($"{dropItem.Name}", dropItem);
										currentMessage = $"Dropped {dropItem.Name}";
									}
									else
									{
										currentMessage = "Drop what?";
									}
									isNewRoom = false;
								}
								else
								{
									currentMessage = "Drop what?";
									isNewRoom = false;
								}
								break;
							#endregion
							#region Use
							/*
						case Verbs.use:
							if (!string.IsNullOrEmpty(target))
							{
								Item useItem;
								if (currentRoom.Items.TryGetValue(target, out useItem) || player.Inventory.TryGetValue(target, out useItem))
								{
									currentMessage = $"Used {useItem.Name}";
								}
								else
								{
									currentMessage = "Use what?";
								}
								isNewRoom = false;
							}
							else
							{
								currentMessage = "Use what?";
								isNewRoom = false;
							}
							break;
							*/
							#endregion
							#region Inventory
							case Verbs.inventory:
								if (player.Inventory.GetEnumerator().MoveNext())
								{
									Console.WriteLine($"\nInventory: ");
									foreach (var item in player.Inventory)
									{
										Console.Write($"- {item.Key}");
										currentMessage = "";
									}
								}
								else
								{
									currentMessage = $"\nYou inventory is empty.";
								}
								isNewRoom = false;
								break;
							#endregion
							#region Inspect Things
							case Verbs.inspect:
								if (Command.IsValid(target))
								{
									if (currentRoom.Items.TryGetValue(target, out Item inspectItem))
									{
										currentMessage = inspectItem.Description;
									}
									else
									{
										currentMessage = $"There is no such thing as a {target}";
									}
									isNewRoom = false;
								}
								else
								{
									currentMessage = "";
									Console.WriteLine($"{currentRoom.FirstDescription}\n");

									if (currentRoom.Items.Count != 0)
									{
										Console.WriteLine($"You look arround and see:\n");
										foreach (var roomItems in currentRoom.Items)
										{
											Message.Description($"A {roomItems.Value.Name} {roomItems.Value.Place}");
										}
									}

									isNewRoom = false;
								}
								break;
							#endregion
							#region Clear Screen
							case Verbs.clear:
								Console.Clear();
								currentMessage = currentRoom.Description;
								isNewRoom = true;
								break;
							#endregion
							#region Help
							case Verbs.help:
								if (!Command.IsValid(target))
								{
									currentMessage = $"You can \"Go\", \"Pick\", \"Drop\", \"Inspect\", \"Clear\", \"Quit\".";
									isNewRoom = false;
								}
								break;
							#endregion
							#region Quit
							case Verbs.quit:
								isPlaying = false;
								break;
							#endregion
							default:
								currentMessage = Command.ErrorMessage(verb);
								isNewRoom = false;
								break;
						}
					}
					else
					{
						currentMessage = Command.ErrorMessage(verb);
						isNewRoom = false;
					}
				}
				else
				{
					isNewRoom = false;
				}
			}
#endregion
		}
	}
}