﻿using System;
using Enums;
using GameElements;


namespace TextAdventureEngine
{
	public static class Message
	{
		/// <summary>
		/// Get the current room's name.
		/// </summary>
		/// <param name="room"></param>
		public static void Name(Room room)
		{
			Console.WriteLine($"You're currently at: {room.Name}");
		}

		/// <summary>
		/// Describe something
		/// </summary>
		/// <param name="message"></param>
		public static void Description(string message)
		{
			if (message != string.Empty)
			{
				Console.WriteLine(message);
			}
		}
	}
}
